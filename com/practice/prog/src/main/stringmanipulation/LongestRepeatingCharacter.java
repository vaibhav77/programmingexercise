package com.practice.prog.src.main.stringmanipulation;

import java.util.HashMap;
import java.util.Map;
/**
 * You are given a string s and an integer k. You can choose any character of the string and change it to any other uppercase English character. You can perform this operation at most k times.
 *
 * Return the length of the longest substring containing the same letter you can get after performing the above operations.
 *
 * Example 1:
 *
 * Input: s = "ABAB", k = 2
 * Output: 4
 * Explanation: Replace the two 'A's with two 'B's or vice versa.
 * */
public class LongestRepeatingCharacter {

    public static int characterReplacement(String s, int k) {
        Map<Character, Integer> hm = new HashMap<>();

        int wstart = 0, max = 1, res = 1;

        for (int i = 0; i < s.length(); i++) {

            hm.put(s.charAt(i), hm.getOrDefault(s.charAt(i), 0) + 1);

            max = Math.max(hm.get(s.charAt(i)), max);

            if (i - wstart + 1 - max > k) {
                hm.put(s.charAt(wstart), hm.get(s.charAt(wstart)) - 1);
                wstart++;
            }

            res = Math.max(res, i - wstart + 1);

        }
        return res;
    }

    public static void main(String[] args){
        String s = "AABABBA";
        int k = 1;
        System.out.println(characterReplacement(s,k));//expected 4
    }
}
