package com.practice.prog.src.main.dynamicpg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MaxSubsequence {


 public static int findSubsequence(int[] input, int size){
     int[] temp = new int [size];
     Arrays.fill(temp,1);
     int maxLength = -1;
     for(int i=1;i<input.length;i++){
         for(int j=0;j<i;j++){

             if (Math.abs(input[i] - input[j]) <= 1
                     && temp[i] < temp[j] + 1)
                 temp[i] = temp[j] + 1;
         }
     }

     for (int j : temp) {
         if (j > maxLength) maxLength = j;
     }

     return maxLength;
 }


    public static void main(String[] args){
        int[] input = {2, 5, 6, 3, 7, 6, 5, 8};
System.out.println(findSubsequence(input,input.length));
    }
}
