package com.practice.prog.src.main.Tree;
/* Class to represent Tree node */
class Node {
    int data;
    Node left, right;

    public Node(int item)
    {
        data = item;
        left = null;
        right = null;
    }
}