package com.practice.prog.src.main.Tree;

// Iterative Queue based Java program
// to do level order traversal
// of Binary Tree

/* importing the inbuilt java classes
   required for the program */
import java.util.ArrayDeque;
import java.util.Deque;



/* Class to print Level Order Traversal */
class BinaryTree {

    Node root;

    /* Given a binary tree. Print
     its nodes in level order/Breadth first  */
    void printLevelOrder()
    {
        Deque<Node> queue = new ArrayDeque<>();
        queue.push(root);
        while (!queue.isEmpty()) {

            /* poll() removes the present head. */
            Node tempNode = queue.remove();
            System.out.print(tempNode.data + " ");

            /*Enqueue left child */
            if (tempNode.left != null) {
                queue.add(tempNode.left);
            }

            /*Enqueue right child */
            if (tempNode.right != null) {
                queue.add(tempNode.right);
            }
        }
    }

    void dfs(Node root){
        if(root == null){
            return;
        }
        System.out.print(root.data +" ");
        dfs(root.left);
        dfs(root.right);
    }

     

    public static void main(String[] args)
    {
        /* creating a binary tree and entering
         the nodes */
        BinaryTree tree_level = new BinaryTree();
        tree_level.root = new Node(1);
        tree_level.root.left = new Node(2);
        tree_level.root.right = new Node(3);
        tree_level.root.left.left = new Node(4);
        tree_level.root.left.right = new Node(5);

        System.out.println("Level order traversal of binary tree is - ");
                tree_level.printLevelOrder();

        System.out.println("dfs of binary tree is - ");
        tree_level.dfs(tree_level.root);
    }
}