package com.practice.prog.src.main.Tree;

import java.util.*;

public class TreeSetSorting {

    public static void main(String[] args)

    {



        Map<Integer, Integer> map
                = new HashMap<>();

        // Put elements to the map
        map.put(1, 10);

        map.put(5, 5);

        map.put(3, 3);

        map.put(2, 2);

        map.put(4, 4);

        // Calling the method valueSort
        LinkedHashMap<Integer, Integer> sortedMap = new LinkedHashMap<>();
        map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEach(x -> sortedMap.put(x.getKey(), x.getValue()));

        // Get a set of the entries on the sorted map
        Set set = sortedMap.entrySet();

        // Get an iterator
        Iterator i = set.iterator();

        while (i.hasNext())
        {
            Map.Entry mp = (Map.Entry)i.next();

            System.out.print(mp.getKey() + ": ");

            System.out.println(mp.getValue());
        }
    }
}

