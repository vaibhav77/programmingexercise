package com.practice.prog.src.main.array;

import java.util.HashMap;
import java.util.Map;

public class SubArraySum {

    public static int subarraysDivByK(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        int runningSum = 0;
        int count = 0;

        map.put(0, 1);

        for(int i=0; i<nums.length; i++) {
            runningSum += nums[i];
            int reminder = runningSum % k;
            if(reminder < 0) {
                reminder += k;
            }
            if(map.containsKey(reminder)) {
                count += map.get(reminder);
            }
            map.put(reminder, map.getOrDefault(reminder, 0)+1);
        }
        return count;
    }

    static int findSubarraySum(int arr[], int n, int sum) {
        // HashMap to store number of subarrays
        // starting from index zero having
        // particular value of sum.
        HashMap<Integer, Integer> prevSum = new HashMap<>();
        prevSum.put(0, 1);
        int res = 0;

        // Sum of elements so far.
        int currSum = 0;

        for (int i = 0; i < n; i++) {

            // Add current element to sum so far.
            currSum += arr[i];
            //calculate the sum that have to be removed
            //so that we can get the desired sum

            int removeSum = currSum - sum;

            //get count of occurrences of that sum that
            //have to removed and add it to res value
            if (prevSum.containsKey(removeSum))
                res += prevSum.get(removeSum);

            // Add currsum value to count of
            // different values of sum.
            prevSum.put(currSum, prevSum.getOrDefault(currSum, 0) + 1);
        }
        return res;
    }

    public static void main(String[] args){
        int[] nums = {4,5,0,-2,-3,1};
        int k = 5;
        System.out.println(subarraysDivByK(nums,k));//expected 7
        //[4, 5, 0, -2, -3, 1], [5], [5, 0], [5, 0, -2, -3], [0], [0, -2, -3], [-2, -3]
        System.out.println(findSubarraySum(nums,nums.length,k));//expected 3
    }
}
