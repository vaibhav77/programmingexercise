package com.practice.prog.src.main.array;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

public class ArraySortingFreq {

    public static int[] frequencySort(int[] nums) {
        HashMap<Integer, Integer> map = new HashMap<>();

        for(int i: nums){
            map.put(i, map.getOrDefault(i,0)+1);
        }

        Comparator<Integer> comparator = (a, b) -> {
            if(map.get(a)<map.get(b)){
                return -1;
            }
            if(map.get(a) > map.get(b)){
                return 1;
            }
            return b-a;
        };
        nums =  Arrays.stream(nums).boxed().sorted(comparator).mapToInt(x -> x).toArray();
map.entrySet().forEach(x-> System.out.println(x.getKey()+":"+x.getValue()));
        return nums;
    }

    public static void main (String[] args){
        int[] input =  {6,6,7,7,9,9,9,3,5,0,0,9,6,0,9,5,5,5,5,0};
        frequencySort(input);
        Arrays.stream(frequencySort(input)).forEach(System.out::println) ;
    }
}
