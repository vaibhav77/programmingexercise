package com.practice.prog.src.main.array;

import java.util.Arrays;

public class ShiftZero {

    public static void main(String[] args){
        //int[] input = {1,2,3,0,0,0,4,5};
        int[] input = {0,2,3,0,0,0,4,5};
        moveZeroes(input);
        Arrays.stream(input).forEach(System.out::println);
    }

    static void moveZeroes(int[] nums){
        int left = 0, right =0, temp;

        while (right < nums.length) {
            if (nums[right] == 0) {
                temp = nums[left];
                nums[left] = nums[right];
                nums[right] = temp;
                left++;
            }
            right++;
        }
    }
}
