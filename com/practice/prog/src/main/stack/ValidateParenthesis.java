package com.practice.prog.src.main.stack;

import java.util.ArrayDeque;
import java.util.Deque;

public class ValidateParenthesis {

  static boolean validate(String input) {

       Deque<Character> parenthesisStack = new ArrayDeque<>();

       char[] charArray = input.toCharArray();

       for (char c : charArray) {

           if (c == '{' || c == '(' || c == '[') {
               parenthesisStack.push(c);

               continue;
           }
           char prevParenthesis = 0;
           if(!parenthesisStack.isEmpty()) {
                prevParenthesis = parenthesisStack.pop();
           }

           switch (c) {

               case '}':
                   if ('{' != prevParenthesis) {
                       return false;
                   }
                   break;
               case ']':
                   if ('[' != prevParenthesis) {
                       return false;
                   }
                   break;
               case ')':
                   if ('(' != prevParenthesis) {
                       return false;
                   }
                   break;
           }


       }
       return parenthesisStack.isEmpty();
   }

   public static void main(String[] args){
       String testInput = "{})";
       System.out.println(validate(testInput));
   }
}
