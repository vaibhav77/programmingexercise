package com.practice.prog.src.main.stack;

import java.util.Stack;

public class HistogramArea
{
    // The main function to find the maximum rectangular area under given
    // histogram with n bars
    static int getMaxArea(int[] hist, int n)
    {

        int maxArea = 0,i=0, top,currentArea;
        Stack<Integer> topStack = new Stack<>();

        while(i<n){
            if(topStack.isEmpty()  || hist[i]>=hist[topStack.peek()]){
                topStack.push(i++);
            }else{
                top = topStack.pop();
                currentArea = hist[top] * (topStack.isEmpty()?i:i-topStack.peek()-1);
                if(currentArea>maxArea)
                    maxArea = currentArea;
            }
        }

        while (!topStack.isEmpty()){
            top = topStack.pop();
            currentArea = hist[top] * (topStack.isEmpty()?i:i-topStack.peek()-1);
            if(currentArea>maxArea)
                maxArea = currentArea;
        }


        return maxArea;

    }

    // Driver program to test above function
    public static void main(String[] args)
    {
        int[] hist = { 6, 2, 5, 4, 5, 1, 6 };
        System.out.println("Maximum area is " + getMaxArea(hist, hist.length));
    }
}