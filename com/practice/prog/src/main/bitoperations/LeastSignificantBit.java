package com.practice.prog.src.main.bitoperations;

public class LeastSignificantBit {

    // Function returns 1 if set, 0 if not
    static boolean LSB(int num, int K)
    {
        return (num & (1 << (K-1))) != 0;

    }

    // Driver code
    public static void main(String[] args)
    {
        int num = 10, K = 4;

        //Function call
        if(LSB(num, K))
            System.out.println("1") ;

        else
            System.out.println("0");
    }
}
